#!/bin/bash
NUM_JOBS=$1
MAPS_PER_HOST=$2
BYTES_PER_MAP=$3


source ./speculate_defaults.sh

if [[ ! -n "$NUM_JOBS" ]] || [[ ! -n "$MAPS_PER_HOST" ]]; then
    echo ""
    echo "Usage: ./eval_tera.sh <number of jobs> <maps per host> <bytes per map>"
    echo ""
    exit
fi

cd ../setup
source ./shared.sh
cd ../eval


echo "--- Evaluating RandTextWrite + WC (Jobs $NUM_JOBS | Maps per host $MAPS_PER_HOST | Bytes per Map $BYTES_PER_MAP)"
GB=$(bc <<< "scale=2; $MAPS_PER_HOST * $NUM_DATANODES * $BYTES_PER_MAP / 1000000000")
echo "Total GB: $GB gb"

function run_rtw_wc() {    
    ID=$1
    SPEC=$2
    THRESH=$3
    RANDWRITE_OUT="$HDFS_ROOT/RandWrite-$1"
    WC_OUT="$HDFS_ROOT/WordCount-$1"
    ./run_randomtextwriter.sh $MAPS_PER_HOST $BYTES_PER_MAP $RANDWRITE_OUT $SPEC $THRESH noprint
    ./run_wordcount.sh $RANDWRITE_OUT $WC_OUT $SPEC $THRESH noprint
}

function run_eval() {
    echo "--- Setting up HDFS"
    remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -rm -r -skipTrash $HDFS_ROOT/RandWrite*"
    remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -rm -r -skipTrash $HDFS_ROOT/WordCount*"
    remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -mkdir -p $HDFS_ROOT"
    echo "--- Kill Outstanding Jobs"
    remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/mapred job -list | grep job_ | awk ' { system("mapred job -kill " $1) } '"
    sleep 5
    spec=$1
    thresh=$2
    clear_log
    for i in $( seq 1 $NUM_JOBS )
    do
        time run_rtw_wc $i $spec $thresh
    done
    save_cpu $2
}

function save_cpu() {
    remote_exec $NAMENODE_SSH_IP "cat $HADOOP_HOME/logs/hadoop-ubuntu-resourcemanager-master.log | grep 'cpu' > ~/wc_cpu/$1.log"
}

function clear_log() {
    remote_exec $NAMENODE_SSH_IP "> $HADOOP_HOME/logs/hadoop-ubuntu-resourcemanager-master.log"
}


function run_wc_sequence() {
    # run_eval RACSpeculator 0
    run_eval RACSpeculator 0
    # sleep 120
    run_eval RACSpeculator 20
    # sleep 120
    run_eval RACSpeculator 40
    # sleep 120
    run_eval RACSpeculator 60
    # sleep 120
    run_eval RACSpeculator 80
    # sleep 120
    run_eval RACSpeculator 100
    # run_eval RACSpeculator -1
    # run_eval DefaultSpeculator 100
}

run_wc_sequence

# auto shutdown gcp commands
# cd ../setup
# ./stop_cluster.sh
# cd ./gcp
# ./stop_gcp.sh
