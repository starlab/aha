#!/bin/bash
cd ../setup
source ./shared.sh

#
# Load simulation based on 10 minute segments of the hour
#
# Load pattern example:
#|   0-10    |   10-20   |   20-30   |   30-40   |   40-50   |   50-60  |  -> repeat from 0-10
#   load A      load B      load A      load B      load A      load B
#                  
# load A: even datanodes stressed nm0 down
# load B: odd datanodes stressed nm1 down

# CYCLE is in minutes
CYCLE=$1
T=$((CYCLE*60))
declare -i k=0
F=2 # number of datanode segments
while :; do
  minute=$(date +"%-M")
  # every 10 minutes we change load type
  if [[ $((minute % $CYCLE)) == 0 ]]; then
    # switch between even/odd dnodes to load every 10 minutes
    load_enabled=$((k % F))
    k=$((k+1))
    declare -i i=0
    for DATANODE_IP in "${DATANODE_SSH_IPS[@]}"
    do
      if [[ $((i % F)) == $load_enabled ]]; then
        if [[ ${i} -eq 0 ]] || [[ ${i} -eq 1 ]]; then
          echo "dnode$i down for ${T} seconds"
          remote_exec $DATANODE_IP "$HADOOP_HOME/bin/yarn --daemon stop nodemanager" &
        else
          CPU=$(bc <<< "${DATANODE_VCORES[$i]} * 60/100")
          # check at least 1 vcore
          if [[ ${CPU} < 1 ]]; then
              CPU=1
          fi
          MEM=$(bc <<< "${DATANODE_MEMS[$i]} * 1000 * 50/100")
          echo "dnode$i stress for ${T} seconds - CPU: ${CPU}vcores MEM: ${MEM}mb"
          ssh -i $SSH_KEY $SSH_USER@$DATANODE_IP screen -d -m "stress --cpu ${CPU} --vm 1 --vm-bytes ${MEM}M --timeout ${T}" &
        fi
      else
        if [[ ${i} -eq 0 ]] || [[ ${i} -eq 1 ]]; then
            echo "dnode$i up"
            remote_exec $DATANODE_IP "$HADOOP_HOME/bin/yarn --daemon start nodemanager" &
        else
          echo "dnode$i idle for ${T} seconds" 
        fi
      fi
      i=$((i+1))
    done
    echo ""
    sleep $((CYCLE*60))
  else
    sleep 1
  fi
done