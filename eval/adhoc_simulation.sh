#!/bin/bash
cd ../setup
source ./shared.sh

#
# Load simulation based on 10 minute segments of the hour
#
# Load pattern example:
#|   0-10    |   10-20   |   20-30   |   30-40   |   40-50   |   50-60  |  -> repeat from 0-10
#   load A        n/a        load B       n/a         n/a         n/a
#
# load A: 50% cpu stress --cpu 1 --timeout 300s
# load B: 50% cpu 50% mem stress --vm 1 --vm-bytes 2024M --timeout 300s
# load C: 100% cpu 50% mem stress --cpu 1 --vm 1 --vm-bytes 2024M --timeout 300s
# n/a: do nothing...


while :; do
  minute=$(date +"%-M")
  # every 10 minutes we change load type
  if [[ $((minute % 1)) == 0 ]]; then
    for DATANODE_IP in "${DATANODE_SSH_IPS[@]}"
    do
        new_load=$((RANDOM % 6))
        case $new_load in
          0)  echo "Datanode $DATANODE_IP start load A"
              # remote_exec_no_wait $NAMENODE_SSH_IP "stress --cpu 1 --timeout 600s"
              ssh -i $SSH_KEY $SSH_USER@$DATANODE_IP screen -d -m "stress --cpu 1 --timeout 60s"
              ;;
          1)  echo "Datanode $DATANODE_IP start load B"
              ssh -i $SSH_KEY $SSH_USER@$DATANODE_IP screen -d -m "stress --vm 1 --vm-bytes 2024M --timeout 60s"
              ;;
          2)  echo "Datanode $DATANODE_IP start load C"
              ssh -i $SSH_KEY $SSH_USER@$DATANODE_IP screen -d -m "stress --cpu 1 --vm 1 --vm-bytes 2024M --timeout 60s"
              ;;
          *)  echo "Datanode $DATANODE_IP remain idle"
              ;;
        esac
    done
    sleep 60s
  fi
  sleep 10s
done

cd ../eval
