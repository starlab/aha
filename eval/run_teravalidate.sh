#!/bin/bash
INPUT=$1
OUTPUT=$2
SPECULATOR_CLASS=$3
SUPERNODE_THRESHOLD=$4

source ./speculate_defaults.sh

if [[ ! -n "$INPUT" ]] || [[ ! -n "$OUTPUT" ]]; then
    echo ""
    echo "Usage: ./run_teravalidate.sh <input_dir> <output_dir> [<speculator classname> <supernode threshold [0-100]> (optional together)]"
    echo ""
    exit
fi

if [[ ! -n $SSH_USER ]]; then
    cd ../setup
    source ./shared.sh $5
    cd ../
fi

echo "--- Submitting TeraValidate (in: $INPUT | out: $OUTPUT | speculator: $SPECULATOR_CLASS | supernode thresh: $SUPERNODE_THRESHOLD)"

echo "--- Running TeraValidate Job"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/yarn jar $HADOOP_EXAMPLES/hadoop-mapreduce-examples-3.3.0.jar teravalidate 
-Dyarn.app.mapreduce.am.resource.memory-mb=1024
-Dyarn.app.mapreduce.am.job.speculator.class=org.apache.hadoop.mapreduce.v2.app.speculate.$SPECULATOR_CLASS 
-Dyarn.app.mapreduce.am.job.supernode-threshold=$SUPERNODE_THRESHOLD 
$INPUT 
$OUTPUT"

echo "--- Done!"