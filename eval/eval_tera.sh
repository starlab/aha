#!/bin/bash
NUM_GB=$1
SPECULATOR_CLASS=$2
SUPERNODE_THRESHOLD=$3

source ./speculate_defaults.sh

if [[ ! -n "$NUM_GB" ]]; then
    echo ""
    echo "Usage: ./eval_tera.sh <number of GB> [<speculator classname> <supernode threshold [0-100]> (optional together)]"
    echo ""
    exit
fi

if [[ ! -n $SSH_USER ]]; then
    cd ../setup
    source ./shared.sh
    cd ../eval
fi

echo "--- Evaluating TeraGen/Sort/Validate ($NUM_GB GB | speculator: $SPECULATOR_CLASS | supernode thresh: $SUPERNODE_THRESHOLD)"

HDFS_ROOT="/user/hdfs"
GEN_OUT="$HDFS_ROOT/TeraGen-$NUM_GB-GB"
SORT_OUT="$HDFS_ROOT/TeraSort-$NUM_GB-GB"
VALIDATE_OUT="$HDFS_ROOT/TeraValidate-$NUM_GB-GB"

echo "--- Setting up HDFS"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -rm -r -skipTrash $HDFS_ROOT/TeraGen*"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -rm -r -skipTrash $HDFS_ROOT/TeraSort*"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -rm -r -skipTrash $HDFS_ROOT/TeraValidate*"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -mkdir -p $HDFS_ROOT"

echo "--- Kill Outstanding Jobs"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/mapred job -list | grep job_ | awk ' { system("mapred job -kill " $1) } '"
sleep 2

function run_tera() {
    time ./run_teragen.sh $NUM_GB $GEN_OUT $SPECULATOR_CLASS $SUPERNODE_THRESHOLD noprint
    time ./run_terasort.sh $GEN_OUT $SORT_OUT $SPECULATOR_CLASS $SUPERNODE_THRESHOLD noprint
    time ./run_teravalidate.sh $SORT_OUT $VALIDATE_OUT $SPECULATOR_CLASS $SUPERNODE_THRESHOLD noprint
}

time run_tera

# echo "--- Result"
# remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -ls $VALIDATE_OUT"
# remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -cat $VALIDATE_OUT/*"