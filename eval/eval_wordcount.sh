#!/bin/bash
MAPS_PER_HOST=$1
BYTES_PER_MAP=$2
SPECULATOR_CLASS=$3
SUPERNODE_THRESHOLD=$4

source ./speculate_defaults.sh

if [[ ! -n "$MAPS_PER_HOST" ]] || [[ ! -n "$BYTES_PER_MAP" ]]; then
    echo ""
    echo "Usage: ./eval_wordcount.sh <maps per host> <bytes per map> [<speculator classname> <supernode threshold [0-100]> (optional together)]"
    echo ""
    exit
fi

if [[ ! -n $SSH_USER ]]; then
    cd ../setup
    source ./shared.sh
    cd ../eval
fi

echo "--- Evaluating RandomTextWriter + WordCount (Maps per host $MAPS_PER_HOST | Bytes per Map $BYTES_PER_MAP | speculator: $SPECULATOR_CLASS | supernode thresh: $SUPERNODE_THRESHOLD)"
GB=$(bc <<< "scale=2;$MAPS_PER_HOST * $NUM_DATANODES * $BYTES_PER_MAP / 1000000000")
echo "Total GB: $GB gb"

HDFS_ROOT="/user/hdfs"
RANDWRITE_OUT="$HDFS_ROOT/RandWrite"
WC_OUT="$HDFS_ROOT/WordCount"

echo "--- Setting up HDFS"
# remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -rm -r -skipTrash $RANDWRITE_OUT"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -rm -r -skipTrash $WC_OUT"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -mkdir -p $HDFS_ROOT"

echo "--- Kill Outstanding Jobs"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/mapred job -list | grep job_ | awk ' { system("mapred job -kill " $1) } '"
sleep 2

function run_rtw_wc() {
    # ./run_randomtextwriter.sh $MAPS_PER_HOST $BYTES_PER_MAP $RANDWRITE_OUT $SPECULATOR_CLASS $SUPERNODE_THRESHOLD noprint
    ./run_wordcount.sh $RANDWRITE_OUT $WC_OUT $SPECULATOR_CLASS $SUPERNODE_THRESHOLD noprint
}

time run_rtw_wc

echo "--- Result"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hadoop fs -ls $WC_OUT"
