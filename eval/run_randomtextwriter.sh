#!/bin/bash
NUM_MAPS=$1
BYTES_PER_MAP=$2
OUTPUT=$3
SPECULATOR_CLASS=$4
SUPERNODE_THRESHOLD=$5

source ./speculate_defaults.sh

if [[ ! -n "$NUM_MAPS" ]] || [[ ! -n "$BYTES_PER_MAP" ]] || [[ ! -n "$OUTPUT" ]]; then
    echo ""
    echo "Usage: ./run_randomtextwriter.sh <# maps> <bytes per map> <output_dir> [<speculator classname> <supernode threshold [0-100]> (optional together)]"
    echo ""
    exit
fi

if [[ ! -n $SSH_USER ]]; then
    cd ../setup
    source ./shared.sh $5
    cd ../
fi

GB=$(bc <<< "scale=2; $NUM_MAPS * $NUM_DATANODES * $BYTES_PER_MAP / 1000000000")

echo "--- Submitting RandomTextWriter (Number Maps $NUM_MAPS | Bytes per map $BYTES_PER_MAP | out: $OUTPUT | speculator: $SPECULATOR_CLASS | supernode thresh: $SUPERNODE_THRESHOLD)"
echo "--- Running RandomTextWriter Job  $GB gb"
remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/yarn jar $HADOOP_EXAMPLES/hadoop-mapreduce-examples-3.3.0.jar randomtextwriter 
-Dmapreduce.randomtextwriter.mapsperhost=$NUM_MAPS
-Dmapreduce.randomtextwriter.bytespermap=$BYTES_PER_MAP
-Dyarn.app.mapreduce.am.resource.memory-mb=1024 
-Dyarn.app.mapreduce.am.job.speculator.class=org.apache.hadoop.mapreduce.v2.app.speculate.$SPECULATOR_CLASS 
-Dyarn.app.mapreduce.am.job.supernode-threshold=$SUPERNODE_THRESHOLD 
$OUTPUT"

echo "--- Done!"






