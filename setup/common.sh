#!bin/bash
#
# NOTE: this script should be executed locally on remote server
#
NAMENODE_IP=$1
VCORES=$2
MEM_GB=$3
NEED_SETUP=$4


HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop
HADOOP_HOME=/usr/local/hadoop

if [[ ! -n $NEED_SETUP ]]; then
    echo "--- Installing dependencies"
    sudo apt-get update -y 
    sudo apt-get upgrade -y 
    sudo apt-get install pdsh -y
    sudo apt-get install stress -y
    sudo apt-get install openjdk-11-jdk-headless -y 
fi

# echo "--- Downloading Hadoop"
# wget -q https://muug.ca/mirror/apache-dist/hadoop/common/hadoop-3.3.0/hadoop-3.3.0.tar.gz 
sudo rm -rf $HADOOP_HOME
echo "--- Extracting Hadoop"
sudo tar zxf ~/hadoop-* -C /usr/local
sudo mv /usr/local/hadoop-3.3.0 /usr/local/hadoop

echo "--- Setting up Environment"
# configure .bashrc
FILE=~/.bashrc_bak
if [ -f "$FILE" ]; then
    :
else 
    # create back up copy - used when re-running this step so .bashrc setup is idempotent
    cp ~/.bashrc ~/.bashrc_bak
fi
cp ~/.bashrc_bak ~/.bashrc_new
echo "export PDSH_RCMD_TYPE=ssh
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export PATH=$PATH:$JAVA_HOME/bin
export PATH=$PATH:$HADOOP_HOME/bin
export HADOOP_CLASSPATH=${JAVA_HOME}/lib/tools.jar
export HADOOP_HOME=/usr/local/hadoop
export HADOOP_COMMON_HOME=$HADOOP_HOME
export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
export HADOOP_EXAMPLES=/usr/local/hadoop/share/hadoop/mapreduce
export HADOOP_HDFS_HOME=$HADOOP_HOME
export HADOOP_MAPRED_HOME=$HADOOP_HOME
export HADOOP_YARN_HOME=$HADOOP_HOME" >> ~/.bashrc_new
cp ~/.bashrc_new ~/.bashrc
source ~/.bashrc

echo "--- Setting up Hadoop (common)"
echo "--- Configure Core"
cp ~/config/core-site.xml ~/config/core-site-tmp.xml
sed -i "s/<nnode>/$NAMENODE_IP/" ~/config/core-site-tmp.xml
sudo mv ~/config/core-site-tmp.xml $HADOOP_CONF_DIR/core-site.xml

echo "--- Configuring YARN"
cp ~/config/yarn-site.xml ~/config/yarn-site-tmp.xml
sed -i "s/<nnode>/$NAMENODE_IP/" ~/config/yarn-site-tmp.xml
MEM_MB=$(bc <<< "$MEM_GB * 1000 * 75/100")
# set resource mem
sed -i "s/<resource-mem>/$MEM_MB/" ~/config/yarn-site-tmp.xml
# set resource vcores
sed -i "s/<resource-vcores>/$VCORES/" ~/config/yarn-site-tmp.xml
# set max allocation mem
sed -i "s/<max-allocation-mb>/$MEM_MB/" ~/config/yarn-site-tmp.xml
# set max allocation vcores
sed -i "s/<max-allocation-vcores>/$VCORES/" ~/config/yarn-site-tmp.xml
sudo mv ~/config/yarn-site-tmp.xml $HADOOP_CONF_DIR/yarn-site.xml

sudo sed -i 's/# export JAVA_HOME=/export JAVA_HOME=\/usr\/lib\/jvm\/java-11-openjdk-amd64/' $HADOOP_CONF_DIR/hadoop-env.sh

