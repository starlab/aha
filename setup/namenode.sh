#!/bin/bash
#
# NOTE: this script should be executed locally on remote server
#
NAMENODE_IP=$1
NUM_DATANODES=$2

HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop
HADOOP_HOME=/usr/local/hadoop

# set hdfs-site
echo "--- Configuring HDFS"
# sed -i "s/<num_datanodes>/$NUM_DATANODES/" ~/config/hdfs-site-namenode.xml
sudo cp ~/config/hdfs-site-namenode.xml $HADOOP_CONF_DIR/hdfs-site.xml
sudo mkdir -p $HADOOP_HOME/data/hdfs/namenode​

echo "--- Configuring MapReduce"
cp ~/config/mapred-site-namenode.xml ~/config/mapred-site-namenode-tmp.xml
sed -i "s/<nnode>/$NAMENODE_IP/" ~/config/mapred-site-namenode-tmp.xml
sudo mv ~/config/mapred-site-namenode-tmp.xml $HADOOP_CONF_DIR/mapred-site.xml

echo "--- Configuring CapacityScheduler"
sudo cp ~/config/capacity-scheduler.xml $HADOOP_CONF_DIR/capacity-scheduler.xml

# set masters
echo "--- Setting Masters"
while IFS= read -r line || [[ -n $line ]]; do
  IFS=' ' read -r -a IPS <<< $line
  NN_IP="${IPS[1]}"
  echo "$NN_IP" >> ~/masters
done < ~/config/namenodes
sudo mv ~/masters $HADOOP_CONF_DIR/masters

# set workers
echo "--- Setting Workers"
while IFS= read -r line || [[ -n $line ]]; do
  IFS=' ' read -r -a DATANODE_IPS <<< $line
  DATANODE_IP="${DATANODE_IPS[1]}"
  echo "$DATANODE_IP" >> ~/workers
done < ~/config/datanodes
sudo mv ~/workers $HADOOP_CONF_DIR/workers

sudo chown -R ubuntu $HADOOP_HOME