#!/bin/bash
source ./shared.sh

function configure_node() {
    IP=$1
    SCRIPT_NAME=$2
    VCORES=$3
    MEM_GB=$4
    remote_exec $IP "bash -s" < ./common.sh $NAMENODE_CONFIG_IP $VCORES $MEM_GB $1
    remote_exec $IP "bash -s" < $SCRIPT_NAME $NAMENODE_CONFIG_IP
}

function setup_hadoop() {
    # run hadoop setup
    echo "--- Setting up Hadoop"

    # cleanup config directory
    remote_exec $NAMENODE_SSH_IP "rm -rf $REMOTE_HOME/config" &
    for DATANODE_IP in "${DATANODE_SSH_IPS[@]}"
    do 
        remote_exec $DATANODE_IP "rm -rf $REMOTE_HOME/config" &
    done
    wait

    # always transfer config (since its not very big)
    echo "--- Transferring Config to Master"
    local_to_remote ./config $NAMENODE_SSH_IP $REMOTE_HOME/config
    for DATANODE_IP in "${DATANODE_CONFIG_IPS[@]}"
    do 
        # transfer config
        echo "--- Transferring Config to Worker $DATANODE_IP"
        remote_exec $NAMENODE_SSH_IP "scp -r $REMOTE_HOME/config $DATANODE_IP:$REMOTE_HOME/config" &
    done
    wait

    if [[ ! -n $1 ]]; then
        # cleanup hadoop build
        remote_exec $NAMENODE_SSH_IP "rm -rf $REMOTE_HOME/hadoop-3.3.0.tar.gz" &
        for DATANODE_IP in "${DATANODE_SSH_IPS[@]}"
        do 
            remote_exec $DATANODE_IP "rm -rf $REMOTE_HOME/hadoop-3.3.0.tar.gz" &
        done
        wait

        echo "--- Transferring Hadoop to Master"
        local_to_remote ../hadoop/hadoop-dist/target/hadoop-3.3.0.tar.gz $NAMENODE_SSH_IP $REMOTE_HOME/hadoop-3.3.0.tar.gz
    fi

    declare -i k=0
    for DATANODE_IP in "${DATANODE_CONFIG_IPS[@]}"
    do  
        if [[ ! -n $1 ]]; then
            # transfer hadoop build from master to workers
            echo "--- Transferring Hadoop to Worker $DATANODE_IP"
            remote_exec $NAMENODE_SSH_IP "scp -r $REMOTE_HOME/hadoop-3.3.0.tar.gz $DATANODE_IP:$REMOTE_HOME/hadoop-3.3.0.tar.gz" &
        else
            # transfer hadoop build if not exist
            if ssh -T -oStrictHostKeyChecking=no -q -i $SSH_KEY $SSH_USER@${DATANODE_SSH_IPS[$k]} test -f "$REMOTE_HOME/hadoop-3.3.0.tar.gz"; then
                echo "--- Already transferred Hadoop to Worker $DATANODE_IP"
            else
                # transfer hadoop build from master to workers
                echo "--- Transferring Hadoop to Worker $DATANODE_IP"
                remote_exec $NAMENODE_SSH_IP "scp -r $REMOTE_HOME/hadoop-3.3.0.tar.gz $DATANODE_IP:$REMOTE_HOME/hadoop-3.3.0.tar.gz" &
            fi
        fi
        k=$((k+1))
    done
    wait

    echo "--- Setting up Master"
    configure_node $NAMENODE_SSH_IP ./namenode.sh $NAMENODE_VCORE $NAMENODE_MEM &
    echo ""
    declare -i x=0
    for DATANODE_IP in "${DATANODE_SSH_IPS[@]}"
    do 
        echo "--- Setting up Worker $DATANODE_IP"
        # transfer files from master to workers
        configure_node $DATANODE_IP ./datanode.sh ${DATANODE_VCORES[$x]} ${DATANODE_MEMS[$x]} &
        x=$((x+1))
        echo ""
    done 
    wait
    
    echo "--- Formatting HDFS"
    remote_exec $NAMENODE_SSH_IP "$HADOOP_HOME/bin/hdfs namenode -format"
    
    # # cleanup 
    # remote_exec $NAMENODE_SSH_IP "rm -rf $REMOTE_HOME/hadoop-3.3.0.tar.gz" &
    # remote_exec $NAMENODE_SSH_IP "rm -rf $REMOTE_HOME/config" &
    # for DATANODE_IP in "${DATANODE_SSH_IPS[@]}"
    # do 
    #     remote_exec $DATANODE_IP "rm -rf $REMOTE_HOME/hadoop-3.3.0.tar.gz" &
    #     remote_exec $DATANODE_IP "rm -rf $REMOTE_HOME/config" &
    # done
    # wait   
    echo ""     
}

# setup ssh connections between nodes in cluster + test connections
./ssh_test.sh noprint

# install and setup hadoop
setup_hadoop $1
