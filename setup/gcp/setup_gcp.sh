#!/bin/bash
source ./gcp_config

# get VM IPs
> ./gcloud_ips
gcloud compute instances list > ./gcp_ips

NAMENODE_CONFIG_IP=""
NAMENODE_SSH_IP=""
DATANODE_CONFIG_IPS=()
DATANODE_SSH_IPS=()

DNODE_OUT=../config/datanodes
NNODE_OUT=../config/namenodes

echo "--- Reset Known Hosts"
> ~/.ssh/known_hosts

function get_dnode_index() {
	NAME=$1
	declare -i k=0
	for N in "${DNODE_NAMES[@]}"
	do
		if [[ "$NAME" == "$N" ]]; then
			break
		fi
		k=$((k+1))
	done
	echo "$k"
}

cat ./gcp_ips 

declare -i i=0
while IFS= read -r line || [[ -n $line ]]; do
	# skip header line (first line)
	if [[ "$i" -eq "0" ]]; then
		i=$((i+1))
		continue
	fi

	split=($line)
    config_ip="${split[3]}"
    ssh_ip="${split[4]}"
    index=$(get_dnode_index ${split[0]})
    if [[ "${split[0]}" == "${NNODE_NAME}" ]]; then
		if [[ "${ssh_ip}" == "TERMINATED" ]]; then
			echo "${split[0]} is not running..."
			exit
		fi
    	NAMENODE_SSH_IP="$ssh_ip"
    	NAMENODE_CONFIG_IP="$config_ip"
    elif [[ "${index}" == "${#DNODE_NAMES[@]}" ]]; then
    	continue
    else
		if [[ "${ssh_ip}" == "TERMINATED" ]]; then
			echo "${split[0]} is not running..."
			exit
		fi
    	DATANODE_SSH_IPS[$index]=$ssh_ip
    	DATANODE_CONFIG_IPS[$index]=$config_ip
    fi
	i=$((i+1))
done < ./gcp_ips

MD_CORE=2
MD_MEM=3.75
MED_COUNT=6

LG_CORE=4
LG_MEM=7.5
LG_COUNT=2

SM_CORE=1
SM_MEM=2.75
SM_COUNT=2

NN_CORE=2
NN_MEM=3.75

echo "--- Create datanodes file"
> $DNODE_OUT
for name in ${DNODE_NAMES[@]}
do
	echo $name
	index=$(get_dnode_index ${name})
	out=""
	if [[ $name == *"medium"* ]]; then
		out="${DATANODE_SSH_IPS[$index]} ${DATANODE_CONFIG_IPS[${index}]} $MD_CORE $MD_MEM"
	elif [[ $name == *"large"* ]]; then
		out="${DATANODE_SSH_IPS[${index}]} ${DATANODE_CONFIG_IPS[$index]} $LG_CORE $LG_MEM"
	elif [[ $name == *"small"* ]]; then
		out="${DATANODE_SSH_IPS[$index]} ${DATANODE_CONFIG_IPS[$index]} $SM_CORE $SM_MEM"
	fi

	if [[ "$index" -eq "${#DNODE_NAMES[@]}-1" ]]; then
		echo -n $out >> $DNODE_OUT
	else
		echo $out >> $DNODE_OUT
	fi
done

echo "--- Create namenodes file"
echo $NNODE_NAME
> $NNODE_OUT
echo -n "$NAMENODE_SSH_IP $NAMENODE_CONFIG_IP $NN_CORE $NN_MEM" >> $NNODE_OUT
