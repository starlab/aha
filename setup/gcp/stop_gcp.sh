#!/bin/bash
source ./gcp_config

function stop_vm() {
	gcloud compute instances stop $1 --zone $2
}
stop_vm $NNODE_NAME $ZONE &
for name in ${DNODE_NAMES[@]}
do
    stop_vm $name $ZONE &
done
wait
