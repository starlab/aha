#!/bin/bash
source ./shared.sh

echo "--- Stopping Hadoop Cluster"
ssh -T -oStrictHostKeyChecking=no -i $SSH_KEY $SSH_USER@$NAMENODE_SSH_IP "export PDSH_RCMD_TYPE=ssh; $HADOOP_HOME/sbin/stop-all.sh"
