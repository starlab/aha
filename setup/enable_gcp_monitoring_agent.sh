#!/bin/bash
#
# NOTE: this should should be run locally on local machine
#
if [[ ! -n $SSH_USER ]]; then
    source ./shared.sh $1
fi

function setup_monitoring_agent() {
    TARGET_IP=$1
    echo "--- Setting up GCP Monitoring agent on $TARGET_IP"

    if ssh -T -oStrictHostKeyChecking=no -q -i $SSH_KEY $SSH_USER@$TARGET_IP test -f "$REMOTE_HOME/add-monitoring-agent-repo.sh"; then
        echo "--- Already installed monitoring agent"
    else
        echo "--- Installing monitoring agent package repository"
        remote_exec $TARGET_IP "curl -sSO https://dl.google.com/cloudagents/add-monitoring-agent-repo.sh"
        remote_exec $TARGET_IP "sudo bash add-monitoring-agent-repo.sh"
        remote_exec $TARGET_IP "sudo apt-get update"
        remote_exec $TARGET_IP "sudo apt-get install -y 'stackdriver-agent=6.*'"
    fi
    echo "--- Starting monitoring agent"
    remote_exec $TARGET_IP "sudo service stackdriver-agent restart"
    echo ""
}


## NOTE: A minimum of 250 MiB of resident (RSS) memory is recommended to run the Monitoring agent.

# install on namenode
setup_monitoring_agent $NAMENODE_SSH_IP

# install on datanode
for DATANODE_IP in "${DATANODE_SSH_IPS[@]}"
do
    setup_monitoring_agent $DATANODE_IP
done


