/**
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package org.apache.hadoop.mapreduce.v2.app.speculate;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import org.apache.hadoop.yarn.api.records.NodeId;
import org.apache.hadoop.yarn.api.records.NodeReport;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.MRJobConfig;
import org.apache.hadoop.mapreduce.v2.api.records.JobId;
import org.apache.hadoop.mapreduce.v2.api.records.TaskAttemptId;
import org.apache.hadoop.mapreduce.v2.api.records.TaskAttemptState;
import org.apache.hadoop.mapreduce.v2.api.records.TaskId;
import org.apache.hadoop.mapreduce.v2.api.records.TaskType;
import org.apache.hadoop.mapreduce.v2.app.AppContext;
import org.apache.hadoop.mapreduce.v2.app.job.Job;
import org.apache.hadoop.mapreduce.v2.app.job.Task;
import org.apache.hadoop.mapreduce.v2.app.job.TaskAttempt;
import org.apache.hadoop.mapreduce.v2.app.job.event.TaskAttemptStatusUpdateEvent.TaskAttemptStatus;
import org.apache.hadoop.mapreduce.v2.app.job.event.TaskEvent;
import org.apache.hadoop.mapreduce.v2.app.job.event.TaskEventType;
import org.apache.hadoop.service.AbstractService;
import org.apache.hadoop.yarn.event.EventHandler;
import org.apache.hadoop.yarn.exceptions.YarnRuntimeException;
import org.apache.hadoop.yarn.util.Clock;
import org.apache.hadoop.mapreduce.v2.app.MRAppMaster;

import com.google.common.annotations.VisibleForTesting;
import org.apache.hadoop.yarn.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// adhoc section start ------------------------------------------------------------------------------------------------------------------------------
public class HistoryBasedSpeculationHelper {
  private static final Logger LOG =
      LoggerFactory.getLogger(HistoryBasedSpeculationHelper.class);
  // idea:
  // 1. do wns calculation in thread
  // 2. remove speculative thread and only have wns-based speculation as below
  
  private MRAppMaster.RunningAppContext context;
  private TaskRuntimeEstimator estimator;
  private final ConcurrentMap<TaskId, List<Float>> taskScores
      = new ConcurrentHashMap<TaskId, List<Float>>(); 

  public HistoryBasedSpeculationHelper(MRAppMaster.RunningAppContext context, 
      TaskRuntimeEstimator estimator) {
    this.context = context;
    this.estimator = estimator;
  }

  public float speculationScore(TaskId taskID, long now) {
    Job job = context.getJob(taskID.getJobId());
    Task task = job.getTask(taskID);
    Map<TaskAttemptId, TaskAttempt> attempts = task.getAttempts();
  
    TaskAttemptId runningTaskAttemptID = null;
    HashMap<NodeId, TaskAttempt> nodeAttemptMap = new HashMap<>();
    
    // build local map of node scores using NEWEST task attempts' values
    for (TaskAttempt taskAttempt : attempts.values()) {
      runningTaskAttemptID = taskAttempt.getID();
      NodeId nodeId = taskAttempt.getNodeId();
      if (nodeId == null) {
        continue;
      }
      if (nodeAttemptMap.containsKey(nodeId)) {
        TaskAttempt existingAttempt = nodeAttemptMap.get(nodeId);
        long taskAttemptStartTime
            = estimator.attemptEnrolledTime(runningTaskAttemptID);
        long existingAttemptStartTime 
            = estimator.attemptEnrolledTime(existingAttempt.getID());
        if (existingAttemptStartTime < taskAttemptStartTime) {
          // overwrite existing attempt since this attempt is newer!
          nodeAttemptMap.put(nodeId, taskAttempt);
        }
      } else {
        nodeAttemptMap.put(nodeId, taskAttempt);
      }
    }

    // calculate total score by combining each TaskAttempt's node score
    float totalUnavailProp = 1.0f;
    int availableNodeScores = 0;    
    for (TaskAttempt taskAttempt : attempts.values()) {
      if (isAttemptReadyForSpeculation(taskAttempt, now) == false) {
        continue;
      }
      // use score from heartbeat to RM
      NodeId currentNodeId = taskAttempt.getNodeId();
      // use score from latest TaskAttempt on the same node
      TaskAttempt latestAttemptOnNode = nodeAttemptMap.get(taskAttempt.getNodeId());
      float currentScore = -1;
      if(latestAttemptOnNode != null) {
        currentScore = latestAttemptOnNode.getNodeScore() / 100.0f;
        LOG.info(">>>>> Attempt score for Node " + currentNodeId +" = " + currentScore);
      }

      // sanity check
      if (currentScore == -1) {
        LOG.info(">>>>> No score found for Node " + currentNodeId);
        continue;
      }
        
      float currentUnavailProp = 1.0f - currentScore;
      totalUnavailProp *= currentUnavailProp;
      ++availableNodeScores;
    }
    float result = availableNodeScores == 0 ? 1 : (1.0f - totalUnavailProp) * 100.0f;
    LOG.info(">>>> Task " + taskID + " total score: " + result);
    return result;

  }

  public boolean isAttemptReadyForSpeculation(TaskAttempt taskAttempt, long now) {
    long taskAttemptStartTime
            = estimator.attemptEnrolledTime(taskAttempt.getID());
    if (taskAttemptStartTime > now) {
      // This background process ran before we could process the task
      //  attempt status change that chronicles the attempt start
      LOG.info(">>>>> TaskAttempt " + taskAttempt.getID() 
      + " too new!!");
      return false;
    } 
    return true;
  }

  public void addScore(TaskId taskID, float score) {
    List<Float> currentScores = taskScores.putIfAbsent(
      taskID, 
      new ArrayList<Float>());
    if (currentScores == null) {
      currentScores = taskScores.get(taskID);  
    }
    currentScores.add(score);
  }

  public boolean alreadySpeculating(TaskId taskID) {
    Job job = context.getJob(taskID.getJobId());
    Task task = job.getTask(taskID);
    Map<TaskAttemptId, TaskAttempt> attempts = task.getAttempts();

    int numberRunningAttempts = 0;
    for (TaskAttempt taskAttempt : attempts.values()) {
      if (taskAttempt.getState() == TaskAttemptState.RUNNING
          || taskAttempt.getState() == TaskAttemptState.STARTING) {
        if (++numberRunningAttempts > 1) {
          return true;
        }
      }
    }
    return false;
  } 

  public boolean alreadyRunning(TaskId taskID) {
    Job job = context.getJob(taskID.getJobId());
    Task task = job.getTask(taskID);
    Map<TaskAttemptId, TaskAttempt> attempts = task.getAttempts();
    int numberRunningAttempts = 0;
    for (TaskAttempt taskAttempt : attempts.values()) {
      if (taskAttempt.getState() == TaskAttemptState.RUNNING
          || taskAttempt.getState() == TaskAttemptState.STARTING) {
        ++numberRunningAttempts;
      }
    }
    return numberRunningAttempts > 0;
  }

  public float getSupernodeThreshold() {
    return ((MRAppMaster.RunningAppContext) this.context).getSupernodeThreshold();
  }

  public boolean needSpeculativeReplication(TaskId taskID, long now) {  
    float threshold = getSupernodeThreshold();
    float totalScore = speculationScore(taskID, now);
    if(totalScore >= threshold) {
      // do nothing
      LOG.info(">>>>> DON'T NEED speculative attempt! threshold: " + threshold);
      return false;
    } else {
      // new spec attempt
      LOG.info(">>>>> NEED speculative attempt! threshold: " + threshold);
      return true;
    }
  }
}
// adhoc section end ------------------------------------------------------------------------------------------------------------------------------

