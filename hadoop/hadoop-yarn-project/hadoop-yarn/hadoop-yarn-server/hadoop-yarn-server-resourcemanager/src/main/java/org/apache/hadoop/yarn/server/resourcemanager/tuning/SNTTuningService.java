/**
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements.  See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership.  The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License.  You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package org.apache.hadoop.yarn.server.resourcemanager.tuning;

import java.io.IOException;
import java.util.HashSet;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.hadoop.classification.InterfaceAudience.Private;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.service.AbstractService;
import org.apache.hadoop.yarn.api.records.NodeId;
import org.apache.hadoop.yarn.api.records.Resource;
import org.apache.hadoop.yarn.client.AMRMClientUtils;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.server.resourcemanager.rmapp.RMApp;
import org.apache.hadoop.yarn.server.resourcemanager.AdminService;
import org.apache.hadoop.yarn.server.resourcemanager.RMContext;
import org.apache.hadoop.yarn.server.resourcemanager.RMContextImpl;
import org.apache.hadoop.yarn.server.resourcemanager.rmnode.RMNodeEvent;
import org.apache.hadoop.yarn.server.resourcemanager.rmnode.RMNodeEventType;
import org.apache.hadoop.yarn.api.records.ResourceUtilization;
import org.apache.hadoop.yarn.server.resourcemanager.rmnode.RMNodeStatusEvent;
import org.apache.hadoop.yarn.server.resourcemanager.rmapp.RMAppState;
import org.apache.hadoop.yarn.event.EventHandler;
import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.rule.Variable;
import org.apache.hadoop.yarn.api.records.ApplicationId;
import org.apache.hadoop.yarn.api.records.ApplicationResourceUsageReport;

import org.apache.hadoop.yarn.server.resourcemanager.scheduler.capacity.CapacityScheduler;
import org.apache.hadoop.yarn.server.resourcemanager.scheduler.YarnScheduler;

@SuppressWarnings("unchecked")
@Private
public class SNTTuningService extends AbstractService implements EventHandler<RMNodeEvent> {
  private static final String FCL_FILE_DIR = "config/";
  private static final String AMRQ_KNOB = "amrp";
  private static final String SUPERNODE_THRESHOLD_KNOB = "supernode-threshold";
  private static final Logger LOG = LoggerFactory.
      getLogger(SNTTuningService.class);

  // hadoop variables
  private CapacityScheduler capacityScheduler;
  protected final RMContext rmContext;
  private final AdminService adminService;

  // tuning variables
  private final ConcurrentHashMap<NodeId, Resource> nodeResourceMap = 
    new ConcurrentHashMap<NodeId, Resource>();
  private final ConcurrentHashMap<NodeId, ResourceUtilization> nodeResourceUtilMap = 
    new ConcurrentHashMap<NodeId, ResourceUtilization>();
  private float prevKnobValue = -1;
  private float prevUtil = -1;
  private float prevNumJobs = -1;
  private String configKnob;
  private String resourceInput;
  private long tuningTimeStep;
  private long currentTimeStep = 0;
  private float knobIncrement;
  private float resourceUtilAggregator = 0;
  private int numJobsAggregator = 0;
  private boolean tuningEnabled = false;
  private float overallocateLimit =1.25f;
  private float underallocateLimit = 0.5f;

  // fuzzy logic variables  
  private FIS fis;
  private FunctionBlock functionBlock;
  private final String CHANGE_IN_RESOURCE = "changeInResourceUsage";
  private final String CHANGE_IN_JOBS = "changeInJobs";
  private final String NEXT_CHANGE_IN_JOB = "nextChangeInJobs";


  // background thread variables
  // flag stopping background thread
  private volatile boolean stopped = false;
  private Thread tuningBackgroundThread = null;
  private Thread resourceAggregateThread = null;


  // event handling variables
  private HashSet<RMNodeEventType> registerEventTypes = new HashSet<>();
  private HashSet<RMNodeEventType> unregisterEventTypes = new HashSet<>();
  private HashSet<RMNodeEventType> updateEventTypes = new HashSet<>();

  public SNTTuningService(String name, RMContext rmContext,
      YarnScheduler scheduler, AdminService adminService) {
    super(name);
    this.adminService = adminService;
    this.capacityScheduler = (CapacityScheduler) scheduler;
    this.rmContext = (RMContextImpl) rmContext;
  }

  @Override
  protected void serviceInit(Configuration conf) throws Exception {
    String fclFilename = conf.get(
      YarnConfiguration.RM_TUNING_FUZZY_CONTROL_FILE,
      YarnConfiguration.DEFAULT_RM_TUNING_FUZZY_CONTROL_FILE
    );
    this.configKnob = conf.get(
      YarnConfiguration.RM_TUNING_CONFIG_KNOB,
      YarnConfiguration.DEFAULT_RM_TUNING_CONFIG_KNOB
    );
    this.resourceInput = conf.get(
      YarnConfiguration.RM_TUNING_RESOURCE_INPUT,
      YarnConfiguration.DEFAULT_RM_TUNING_RESOURCE_INPUT
    );
    this.tuningTimeStep = conf.getLong(
      YarnConfiguration.RM_TUNING_TIMESTEP,
      YarnConfiguration.DEFAULT_RM_TUNING_TIMESTEP
    ); // convert to milliseconds
    this.knobIncrement = conf.getFloat(
      YarnConfiguration.RM_TUNING_KNOB_INCREMENT,
      YarnConfiguration.DEFAULT_RM_KNOB_INCREMENT
    );
    this.tuningEnabled = conf.getBoolean(
      YarnConfiguration.RM_TUNING_ENABLED,
      YarnConfiguration.DEFAULT_RM_TUNING_ENABLED
    );
    this.underallocateLimit = conf.getFloat(
      YarnConfiguration.RM_TUNING_UNDERALLOC_LIMIT,
      YarnConfiguration.DEFAULT_RM_TUNING_UNDERALLOC_LIMIT
    );
    this.overallocateLimit = conf.getFloat(
      YarnConfiguration.RM_TUNING_OVERALLOC_LIMIT,
      YarnConfiguration.DEFAULT_RM_TUNING_OVERALLOC_LIMIT
    );
    LOG.info(">>>>> config knob: " + configKnob + ", resource: " 
    + resourceInput + ", timestep: " + tuningTimeStep + "s, enabled: " + tuningEnabled
    + "underalloc limit: " + underallocateLimit + ", overalloc limit: " + overallocateLimit);

    // init fuzzy logic components
    this.fis = FIS.load(FCL_FILE_DIR + fclFilename, true);
    // Error while loading?
    if( fis == null ) { 
      LOG.info(">>>>> Could not load file: '" + FCL_FILE_DIR + fclFilename + "'");
    } 
    LOG.info(">>>>> FCL file: " + FCL_FILE_DIR + fclFilename + " loaded!");
    // there should only be 1 function block -> pass null to get first 1
    this.functionBlock = fis.getFunctionBlock(null);

    // setup event handling
    this.registerEventTypes.add(RMNodeEventType.STARTED);
    this.registerEventTypes.add(RMNodeEventType.RECONNECTED);

    this.unregisterEventTypes.add(RMNodeEventType.DECOMMISSION);
    this.unregisterEventTypes.add(RMNodeEventType.GRACEFUL_DECOMMISSION);
    this.unregisterEventTypes.add(RMNodeEventType.SHUTDOWN);
    // this.unregisterEventTypes.add(RMNodeEventType.EXPIRE);

    this.updateEventTypes.add(RMNodeEventType.STATUS_UPDATE);
    // this.updateEventTypes.add(RMNodeEventType.RESOURCE_UPDATE);
  }

  @Override
  protected void serviceStart() throws Exception {
    Configuration conf = getConfig();
    Runnable tuningBackgroundCore
        = new Runnable() {
            @Override
            public void run() {
              while (!stopped && !Thread.currentThread().isInterrupted()) {
                try { 
                  // wait for next time step
                  Thread.sleep(1000);
                  currentTimeStep++;
                  // aggregate resource usage over period
                  float currentResourceUtil = getClusterResourceUtilization(
                    nodeResourceUtilMap);
                  resourceUtilAggregator += currentResourceUtil;   
                  numJobsAggregator += getCurrentNumJobUnits();               
                  if (currentTimeStep >= tuningTimeStep) {
                    // run fuzzy tuning logic
                    maybeAdjustKnob();

                    // reset timer
                    currentTimeStep = 0;
                  }
                } catch (InterruptedException e) {
                  if (!stopped) {
                    LOG.error("Background thread returning, interrupted", e);
                  }
                  return;
                }
              }
            }
          };
    tuningBackgroundThread = new Thread
        (tuningBackgroundCore, "RM Tuning background thread");
    tuningBackgroundThread.start();

    super.serviceStart();
  }
  
  @Override
  protected void serviceStop() throws Exception {
    stopped = true;
    // this could be called before background thread is established
    if (tuningBackgroundThread != null) {
      tuningBackgroundThread.interrupt();
    }
    super.serviceStop();
  }

  @Override
  public void handle(RMNodeEvent event) {
    RMNodeEventType eventType = event.getType();
    if(this.registerEventTypes.contains(eventType)) {
      LOG.info(">>>>> Tuning event: " 
        + eventType + " register Node " + event.getNodeId());
      NodeId nodeId = event.getNodeId();
      registerNode(
        nodeId, 
        this.rmContext.getRMNodes().get(nodeId).getTotalCapability()
      );
    } else if(this.unregisterEventTypes.contains(eventType)) {
      LOG.info(">>>>> Tuning event: " 
        + eventType + " unregister Node " + event.getNodeId());
      unregisterNode(event.getNodeId());
    } else if(this.updateEventTypes.contains(eventType)) {
      // LOG.info(">>>>> Tuning event: " 
      //   + eventType + " update Node " + event.getNodeId());
      updateNodeResourceUtilization(
        event.getNodeId(),
        ((RMNodeStatusEvent)event).getNodeUtilization()
      );
    }
  }

  public synchronized void registerNode(NodeId nodeId, Resource capability) {
    this.nodeResourceMap.put(nodeId, capability);
  }

  private synchronized void unregisterNode(NodeId nodeId) {
    if (this.nodeResourceUtilMap.containsKey(nodeId)) {
      this.nodeResourceUtilMap.remove(nodeId);
    }
    if (this.nodeResourceMap.containsKey(nodeId)) {
      this.nodeResourceMap.remove(nodeId);
    }
  }

  private synchronized void updateNodeResourceUtilization(NodeId nodeId, 
      ResourceUtilization resourceUtil) {
    this.nodeResourceUtilMap.put(nodeId, resourceUtil);
  }

  private void maybeAdjustKnob() {
    float currentNumJobs = numJobsAggregator / (float) tuningTimeStep;
    float currentKnobValue = getCurrentKnobValue();
    // calculate average
    float currentUtil = resourceUtilAggregator / (float) tuningTimeStep;
    // reset value
    resourceUtilAggregator = 0;
    numJobsAggregator = 0;

    // check to make sure there is valid data at previous timestep
    if (prevUtil < 0 || this.prevKnobValue < 0 || this.prevNumJobs < 0) {
      this.prevKnobValue = currentKnobValue;
      this.prevUtil = currentUtil;
      this.prevNumJobs = currentNumJobs;
      return;
    }

    // run controller logic
    float dy = currentUtil - this.prevUtil;
    float du = currentNumJobs - this.prevNumJobs;
    this.fis.setVariable(CHANGE_IN_RESOURCE, dy);
    this.fis.setVariable(CHANGE_IN_JOBS, du);

    // evalulate 
    this.fis.evaluate();
    double posMembership = this.functionBlock.getVariable(NEXT_CHANGE_IN_JOB)
                            .getMembership("positive");
    double negMembership = this.functionBlock.getVariable(NEXT_CHANGE_IN_JOB)
                            .getMembership("negative");
    double zeroMembership = this.functionBlock.getVariable(NEXT_CHANGE_IN_JOB)
                            .getMembership("zero");
    double maxMembership = Math.max(zeroMembership, Math.max(posMembership, negMembership));
    double output = this.functionBlock.getVariable(NEXT_CHANGE_IN_JOB).getValue();
    // LOG.info(">>>>> out: " + this.functionBlock.getVariable(NEXT_CHANGE_IN_JOB).toString());
    LOG.info(">>>>> " + this.configKnob + ": " + currentKnobValue +  
    ", resourceUsage: " + currentUtil + ", numJobs: " + currentNumJobs 
    + ", changeInResourceUsage: " + dy + ", changeInJobs " + du +", nextChangeInJobs: " + output
    +", +: " + posMembership + ", -: " + negMembership + ", 0: " + zeroMembership + ", max: " + maxMembership);

    if (maxMembership == negMembership || currentUtil >= overallocateLimit) {
      if (currentUtil >= overallocateLimit) LOG.info(">>>>> OVERALLOC!");
      currentKnobValue -= this.knobIncrement;
      LOG.info(">>>>> decr " + this.configKnob + " by " + this.knobIncrement);
      setKnobValue(currentKnobValue);
    } else if (maxMembership == posMembership || currentUtil <= underallocateLimit) {
      if (currentUtil <= overallocateLimit) LOG.info(">>>>> UNDERALLOC!");
      LOG.info(">>>>> incr " + this.configKnob + " by " + this.knobIncrement);
      currentKnobValue += this.knobIncrement;
      setKnobValue(currentKnobValue);
    } else {
      LOG.info(">>>>> maintaining " + this.configKnob + " at " + currentKnobValue);
    }
    
    // store current knob value as previous knob value for next time step
    this.prevKnobValue = this.tuningEnabled ? getCurrentKnobValue() : currentKnobValue; 
    this.prevUtil = currentUtil;
    this.prevNumJobs = currentNumJobs;
  }

  private synchronized int getCurrentNumJobUnits() {
    int counter = 0;
    for(ConcurrentMap.Entry<ApplicationId, RMApp> appEntry : 
      this.rmContext.getRMApps().entrySet()) {
      RMAppState appState = appEntry.getValue().getState();
      if (appState == RMAppState.RUNNING) {
        ApplicationResourceUsageReport usageReport = 
          appEntry.getValue()
          .getCurrentAppAttempt()
          .getApplicationResourceUsageReport();
        if(usageReport.getNumUsedContainers() > 0) {
          counter += usageReport.getNumUsedContainers();
        } 
        // if(usageReport.getNumReservedContainers() > 0) {
        //   counter += usageReport.getNumReservedContainers();
        // }
      }
    }
    return counter;
  }

  private synchronized float getCurrentKnobValue() {
    if (this.configKnob == AMRQ_KNOB) {
      return this.capacityScheduler.getConfiguration()
        .getMaximumApplicationMasterResourcePerQueuePercent("root.default");
    } else if (this.configKnob == SUPERNODE_THRESHOLD_KNOB) {
      // divide by 100 because supernode threshold range [0,100]
      return this.rmContext.getSupernodeThreshold() / 100f;
    }
    return -1;
  }

  public synchronized void setKnobValue(float adjustedValue) {
    // if(this.tuningEnabled == false) {
    //   LOG.info(">>>>> Would have set " + this.configKnob + " to " + adjustedValue);
    //   return;
    // }
    if (this.configKnob == AMRQ_KNOB) {
      this.capacityScheduler.getConfiguration()
      .setMaximumApplicationMasterResourcePerQueuePercent("root.default", adjustedValue);
      try {
        this.adminService.refreshQueues();
      } catch (Exception e) {
        LOG.info(">>>>> error refreshing queues: " + e.getMessage());
      }
    } else if (this.configKnob == SUPERNODE_THRESHOLD_KNOB) {
      // multiple by 100 because supernode threshold range [0,100]
      this.rmContext.setSupernodeThreshold(adjustedValue * 100f);
    }
  }

  private synchronized float getClusterResourceUtilization(
      ConcurrentHashMap<NodeId, ResourceUtilization> map) {
    if (map.size() == 0) {
      return 0;
    }
    float used = getUtilizedResources(map);
    float total = getTotalResources(map);
    float utilization = (used / total);
    // LOG.info(">>>> " + this.resourceInput + " used: " + used + " total: " + total);
    return utilization;
  }

  private synchronized float getTotalResources(
    ConcurrentHashMap<NodeId, ResourceUtilization> map) {
    float result = 0;
    for(ConcurrentMap.Entry<NodeId, ResourceUtilization> nodeEntry : map.entrySet()) {
      if (this.resourceInput == "memory") {
        result += this.nodeResourceMap.get(nodeEntry.getKey()).getMemorySize();
      } else {
        // default is cpu
        result += this.nodeResourceMap.get(nodeEntry.getKey()).getVirtualCores();
      }
    }
    return result;
  }

  private synchronized float getUtilizedResources(
    ConcurrentHashMap<NodeId, ResourceUtilization> map) {
    float result = 0;
    for(ConcurrentMap.Entry<NodeId, ResourceUtilization> nodeEntry : map.entrySet()) {
      if (this.resourceInput == "memory") {
        result += map.get(nodeEntry.getKey()).getPhysicalMemory();
      } else {
        // default is cpu
        float vCores = (float) this.nodeResourceMap.get(nodeEntry.getKey())
                                                  .getVirtualCores();
        float vCoreUtil = map.get(nodeEntry.getKey()).getCPU();
        result += (vCores * vCoreUtil);
      }
    }
    return result;
  }
}
