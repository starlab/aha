package org.apache.hadoop.yarn.server.nodemanager.localhistory;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/*
 *This class represents the persistence store for history data keeping 
 */

public class LocalHistoricalDataStore {
	
	//store the equivalent of One month of historical values (default value);
	public static int maximumCapacity = 4;
	
	//the path to the history persistence file
	public static String historyFilePath = "HadoopHistory/";
	
	//persistence threshold (defaults to every one hour)
	public static long persistenceInterval= 30*60*1000;

	private static LocalHistoricalDataStore instance;
	
	private static Map<Integer, ArrayList<Integer>> historicalDataStore = loadMap();
	private static Date lastPersisted = null; 
	
	public static LocalHistoricalDataStore getInstance(){
		if(instance == null){
			instance = new LocalHistoricalDataStore();
		}
		return instance;
	}
	
	public static void store(int timeIntervalId, int score){
		ArrayList<Integer> correspondingReccord = historicalDataStore.get(timeIntervalId);
		if(correspondingReccord == null){
			correspondingReccord = new ArrayList<Integer>(maximumCapacity);
		}
		//keep most recent history records at the top
		correspondingReccord.add(0, score);
		
		//ensure history keeping capacity is not exceeded
		if(correspondingReccord.size() > maximumCapacity){			
			correspondingReccord.remove(maximumCapacity);
		}
		historicalDataStore.put(timeIntervalId, correspondingReccord);
		persist();
	}
	
	private static Map<Integer, ArrayList<Integer>> loadMap() {
		Map<Integer, ArrayList<Integer>> historyStore = loadPersistedStore();
		if(historyStore == null){
			historyStore = new HashMap<Integer, ArrayList<Integer>>();
		}
		return historyStore;
	}

	public static ArrayList<Integer> retrieve(int timeIntervalId){
		return historicalDataStore.get(timeIntervalId);
	}

	public static void persist(){
		
		if (needPersistence()) {
			if (historicalDataStore != null) {
				File historyFile = getDataFile();
				try {
					ObjectOutputStream objectOutputStream = new ObjectOutputStream(
							new FileOutputStream(historyFile));
					objectOutputStream.writeObject(historicalDataStore);
					lastPersisted = new Date(); 
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	private static boolean needPersistence() {
		if (lastPersisted == null){
			return true;
		} else if(new Date().getTime() - lastPersisted.getTime() >= persistenceInterval){
			return true;
		} else{			
			return false;
		}
	}

	//for testing
	public static void main(String... args){
//		store(11, 111111);
//		store(22, 22222222);
//		store(33, 33333333);
//		persist();
		
//		System.out.println(retrieve(11));
	}
	
	private static Map<Integer, ArrayList<Integer>> loadPersistedStore(){
		File historyFile = getDataFile();
		Map<Integer, ArrayList<Integer>> historyMap = null;

		try {
			ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(historyFile));
			historyMap = (Map<Integer, ArrayList<Integer>>) objectInputStream.readObject();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (EOFException e1) {
			return null;
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}	
		return historyMap;
	}
	
	private static File getDataFile(){
		File path = new File(historyFilePath);
		if(!path.exists()){
			path.mkdirs();
		}
		
		File historyFile = new File(path, "historyDataFile");
		try {
			historyFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return (historyFile.exists() ?  historyFile :  null) ; 
	}
}
